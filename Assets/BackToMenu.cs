﻿using UnityEngine;
using System.Collections;

public class BackToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(Menu());
	}

	IEnumerator Menu() {
		yield return new WaitForSeconds(10);
		Application.LoadLevel (0);
	}
}
