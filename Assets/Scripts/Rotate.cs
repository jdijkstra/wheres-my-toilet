﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public float rotationAmount;

	// Update is called once per frame
	void Update () 
	{
		transform.Rotate (0,0,rotationAmount * Time.deltaTime);
	}
}
