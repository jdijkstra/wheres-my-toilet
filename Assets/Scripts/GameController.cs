﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ClothingItem {
	public Sprite sprite;

	public RangeAttribute hue;
	public RangeAttribute saturation;
	public RangeAttribute value;



	public ClothingItem(Sprite sprite) {
		this.sprite = sprite;

		hue = new RangeAttribute(0, 360);
		saturation = new RangeAttribute(0, 100);
		value = new RangeAttribute(0, 100);
	}

	private float RandomValue(RangeAttribute attr) {
		return Random.Range(attr.min, attr.max);
	}

	public Color RandomColor() {
		return GameController.HsvColor(RandomValue(hue), RandomValue(saturation), RandomValue(value));
	}
}

[System.Serializable]
public class ClothingLayer {
	public bool required;
	public List<ClothingItem> sprites;
	public float chanceOfNothing;
	
	public ClothingLayer(bool required, float chanceOfNothing) {
		this.required = required;
		this.sprites = new List<ClothingItem>();
		this.chanceOfNothing = chanceOfNothing;
	}

	public ClothingItem GetRandomClothing() {
		if (!required && Random.value < chanceOfNothing) {
			return null;
		}
		return sprites[Random.Range(0, sprites.Count)];
	}
}

public class GameController : MonoBehaviour {

	public GameObject[] allClothing; // all clothing pieces

	private List<ClothingLayer> allClothingSprites;

	public GameObject[] people;

	private int currentPerson = -1;

	public AudioClip conveyorClip;

	public AudioClip[] Music;
	public AudioClip elevatorClip;

	private AudioSource conveyorSound;

	private AudioSource elevatorSound;

	private bool CreatePlayer = true;

	void Awake()
	{
		LoadClothing();
	}

	// Use this for initialization
	void Start () {
		//PickRandomMusic();
		conveyorSound = gameObject.AddComponent<AudioSource>();
		conveyorSound.clip = conveyorClip;
		conveyorSound.playOnAwake = false;
		conveyorSound.loop = true;
		conveyorSound.volume = 0.1f;

		elevatorSound = gameObject.AddComponent<AudioSource>();
		elevatorSound.volume = 0.2f;
		elevatorSound.clip = elevatorClip;
		elevatorSound.playOnAwake = false;
		elevatorSound.loop = true;

		audio.Play ();



	}

	void PickRandomMusic()
	{
		int bliebloo = Music.Length;
		int randomAmount = Random.Range(0,bliebloo);
		audio.clip = Music [randomAmount];
		audio.Play ();
	}

	void Update()
	{
		//if (audio.isPlaying == false) 
		//{
		//	Debug.Log("playing");
		//	PickRandomMusic();
		//}

		if (CreatePlayer == true)
		{
			SummonNextPerson ();
			CreatePlayer = false;
		}
	}

	private void LoadClothing() {
		allClothingSprites = new List<ClothingLayer>();

		string[] layers = {"Shoes", "Shirt", "Pants", "Coats", "Beards", "Accessories", "Hair", "Hats"};
		List<string> requiredLayers = new List<string>(new string[] {"Shoes", "Pants", "Shirt"});

		Dictionary<string, float> chancesOfNothing = new Dictionary<string, float>();
		chancesOfNothing.Add("Beards", 0.8f);
		chancesOfNothing.Add("Hats", 0.6f);
		chancesOfNothing.Add("Coats", 0.85f);

		foreach (string layerName in layers) {
			Debug.Log("Loading " + layerName);
			float chanceOfNothing = 0.25f;
			if (chancesOfNothing.ContainsKey(layerName)) {
				chanceOfNothing = chancesOfNothing[layerName];
			}
			ClothingLayer layer = new ClothingLayer(requiredLayers.Contains(layerName), chanceOfNothing);
			foreach (Sprite sprite in Resources.LoadAll<Sprite>("Images/Clothing/" + layerName)) {
				layer.sprites.Add(new ClothingItem(sprite));
				Debug.Log("Loaded " + sprite.name);
			}
			if (layer.sprites.Count > 0) {
				allClothingSprites.Add (layer);
			}
		}
	}

	public int ClothingLayersCount() {
		return allClothingSprites.Count;
	}

	public ClothingItem ClothingAtLayer(int layer) {
		return allClothingSprites[layer].GetRandomClothing();
	}
	
	public void SummonNextPerson() {
		currentPerson = (currentPerson + 1) % people.Length;

		Instantiate(people[currentPerson], Vector3.zero, Quaternion.identity);
	}

	public void EndGame()
	{
		Application.LoadLevel (1);
	}

	public void StartConveyor() {
		conveyorSound.Play();
	}

	public void StopConveyor() {
		conveyorSound.Stop();
	}

	public void StartElevator(float duration) {
		elevatorSound.pitch = 2.25f / duration;
		elevatorSound.Play();
	}

	public void StopElevator() 
	{
		elevatorSound.Stop();
	}

	public static Vector3 CameraToWorld(Vector3 position) 
	{
		return Camera.main.ViewportToWorldPoint(position);
	}

	public static Color HsvColor(float h, float s, float v)
	{
		float S = s / 100f;
		float V = v / 100f;

		// ######################################################################
		// T. Nathan Mundhenk
		// mundhenk@usc.edu
		// C/C++ Macro HSV to RGB
		
		float H = h;
		while (H < 0) { H += 360; };
		while (H >= 360) { H -= 360; };
		float R, G, B;
		if (V <= 0)
		{ R = G = B = 0; }
		else if (S <= 0)
		{
			R = G = B = V;
		}
		else
		{
			float hf = H / 60.0f;
			int i = Mathf.FloorToInt(hf);
			float f = hf - i;
			float pv = V * (1 - S);
			float qv = V * (1 - S * f);
			float tv = V * (1 - S * (1 - f));
			switch (i)
			{
				
				// Red is the dominant color
				
			case 0:
				R = V;
				G = tv;
				B = pv;
				break;
				
				// Green is the dominant color
				
			case 1:
				R = qv;
				G = V;
				B = pv;
				break;
			case 2:
				R = pv;
				G = V;
				B = tv;
				break;
				
				// Blue is the dominant color
				
			case 3:
				R = pv;
				G = qv;
				B = V;
				break;
			case 4:
				R = tv;
				G = pv;
				B = V;
				break;
				
				// Red is the dominant color
				
			case 5:
				R = V;
				G = pv;
				B = qv;
				break;
				
				// Just in case we overshoot on our math by a little, we put these here. Since its a switch it won't slow us down at all to put these here.
				
			case 6:
				R = V;
				G = tv;
				B = pv;
				break;
			case -1:
				R = V;
				G = pv;
				B = qv;
				break;
				
				// The color is not defined, we should throw an error.
				
			default:
				//LFATAL("i Value error in Pixel conversion, Value is %d", i);
				R = G = B = V; // Just pretend its black/white
				break;
			}
		}

		return new Color(R, G, B);
	}
	
}
