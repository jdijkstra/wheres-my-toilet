﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {


	private bool closed;

	private Vector3 basePosition;
	
	private Vector3 raisedPosition;

	private float speed;
	
	void Start () {
		basePosition = transform.position;
		raisedPosition = basePosition + (Vector3.up * 5);
		closed = true;
		speed = 4f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Open() {
		if (closed) {
			StopAllCoroutines();
			StartCoroutine(RaiseDoor ());
		}
	}

	IEnumerator RaiseDoor() {
		closed = false;
		while ((transform.position - raisedPosition).magnitude > 0) {
			transform.position = Vector3.Lerp(transform.position, raisedPosition, Time.deltaTime * speed);
			yield return null;
		}
	}

	public void Close() {
		if (!closed) {
			StopAllCoroutines();
			StartCoroutine(LowerDoor());
		}
	}

	IEnumerator LowerDoor() {
		while ((transform.position - basePosition).magnitude > 0.1f) {
			transform.position = Vector3.Lerp(transform.position, basePosition, Time.deltaTime * speed);
			yield return null;
		}
		transform.position = basePosition;
		closed = true;
	}
	
}
