﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class VoiceActor
{
	public AudioClip[] voice;
};


public class Person : MonoBehaviour {

	public int gender = -2; // -1 = geen 0 = man 100 = vrouw
	//public GameObject[] allClothing; // all clothing pieces
	public int[] clothings; // on character

	public int voiceActor = 0; // this is for the randomizer
	public VoiceActor[] voices;

	public AudioClip[] RobotSound;
	public bool isRandomized = true;

	public GameObject baseBody;
	public GameObject baseFace;
	public GameObject baseEyeWhites;
	public GameObject baseEyeBrows;

	public GameObject gameControllerObject;
	private GameController gameController;
	
	private float standPositionY;
	private float startPositionY;
	
	private float timeToGoDown = 10f;
	
	private bool onTheMove = false;
	private bool descending = false;

	private GameObject reticule;

	public int PeePower = 10;
	// Use this for initialization
	void Start()
	{
		switch (voiceActor) 
		{
		case 1:
			gender = 90;
			break;
		case 2:
			gender = 100;
			break;
		case 3:
			gender = 80;
			break;
		case 4:
			gender = 85;
			break;
		case 5:
			gender = 10;
			break;
		case 6:
			gender = 30;
			break;
		case 7:
			gender = 0;
			break;
		case 8:
			gender = 50;
			break;
		case 9:
			gender = 50;
			break;
		case 10:
			gender = 50;
			break;
		case 11:
			gender = 50;
			break;
		case 12:
			gender = 50;
			break;
		default:
			break;
		}
		gender = gender + Random.Range (-100, 100);

		standPositionY = GameController.CameraToWorld(new Vector3(0.5f, 0.3f)).y;
		startPositionY = GameController.CameraToWorld(new Vector3(0.5f, 1f)).y;

		LoadVoice ();
		reticule = GameObject.FindWithTag ("reticule");
		gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent<GameController>();
		if (gameController != null)
		{
			GameObject body = Instantiate(baseBody, new Vector3(0, 0, 10), Quaternion.identity) as GameObject;

			int h = 0;
			int s = 0;
			int v = 0;

			// colours picked from http://colorizer.org/
			switch (Random.Range(1, 4)) {
			case 1:	// brown
				h = 36;
				s = Random.Range (25, 75);
				v = Random.Range (25, 75);
				break;
			case 2: // yellow
				h = 49;
				s = Random.Range (27, 100);
				v = Random.Range (50, 100);
				break;
			case 3: // pink
				h = 21;
				s = Random.Range (12, 28);
				v = Random.Range (80, 100);
				break;
			case 4: // red
				h = 16;
				s = Random.Range (25, 60);
				v = Random.Range (50, 60);
				break;
			}
			body.renderer.material.color = GameController.HsvColor(h, s, v);
			body.transform.parent = transform;

			GameObject eyeWhites = Instantiate(baseEyeWhites, new Vector3(0, 0, 9.5f), Quaternion.identity) as GameObject;
			eyeWhites.transform.parent = transform;
			
			GameObject eyeBrows = Instantiate(baseEyeBrows, new Vector3(0, 0, -10f), Quaternion.identity) as GameObject;
			eyeBrows.renderer.material.color = new Color(99f / 255f, 62f / 255f, 14f / 255f);
			eyeBrows.transform.parent = transform;
			
			GameObject face = Instantiate(baseFace, new Vector3(0, 0, 9), Quaternion.identity) as GameObject;
			switch (Random.Range(1, 2)) {
			case 1:	// green through blue
				h = Random.Range(110, 240);
				s = Random.Range (60, 100);
				v = Random.Range (60, 100);
				break;
			case 2: // brown
				h = 40;
				s = Random.Range (80, 100);
				v = Random.Range (20, 40);
				break;
			}
			face.renderer.material.color = GameController.HsvColor(h, s, v);
			face.transform.parent = transform;

			if (isRandomized) {
				for (int layer = 0; layer < gameController.ClothingLayersCount(); layer++) {
					ClothingItem cloth = gameController.ClothingAtLayer(layer);
					if (cloth != null) {
						GameObject clothObject = Instantiate(baseBody, new Vector3(0, 0, 8 - layer), Quaternion.identity) as GameObject;
						clothObject.GetComponent<SpriteRenderer>().sprite = cloth.sprite;
						clothObject.renderer.material.color = cloth.RandomColor();
						clothObject.transform.parent = transform;
					}
				}

			} else {
				foreach (int cloth in clothings)
				{
					GameObject clothingObject = gameController.allClothing[cloth];
					Clothing Cpiece = clothingObject.transform.GetComponent<Clothing>();
					GameObject clone = Instantiate(clothingObject, new Vector3(Cpiece.pos_X,Cpiece.pos_Y,0),transform.rotation) as GameObject;
					clone.transform.parent = this.transform;
				}
			}
		}
		if (gender == -2) 
		{
			Debug.Log("assign a gender pleaasseee!!!!");
		}
		StartCoroutine(MoveToStart());
	}

	void OnMouseDown()
	{
		if (!onTheMove) {
			PlayAwnser (Random.Range (0,2));
		}
	}

	void LoadVoice()
	{
		if (voiceActor == -1 && isRandomized) 
		{
			Debug.Log("voices.Length: " + voices.Length);
			voiceActor = Random.Range(0,voices.Length);
			Debug.Log("hi");
		}
	}

	void OnMouseEnter()
	{
		if (!onTheMove) {
			foreach (SpriteRenderer renderer in reticule.GetComponentsInChildren<SpriteRenderer>()) {
				renderer.enabled = true;
			}
		}
	}

	void OnMouseExit()
	{
		foreach (SpriteRenderer renderer in reticule.GetComponentsInChildren<SpriteRenderer>()) {
			renderer.enabled = false;
		}
	}

	public void PlayAwnser(int questionNumber)
	{
		if(questionNumber > -1 && questionNumber < 3)
		{
			StartCoroutine(PlayQuestion(questionNumber));
		}
	}	

	IEnumerator PlayQuestion(int questionNumber) {
		audio.PlayOneShot (RobotSound [questionNumber], 1.0f);
		yield return new WaitForSeconds(RobotSound [questionNumber].length + 0.5f);
		audio.PlayOneShot (voices [voiceActor].voice [questionNumber], 1.0F);
	}

	IEnumerator MoveToStart()
	{
		Vector3 currentPos = new Vector3(0, startPositionY, 0);
		float t = 0f;
		transform.tag = "CurrentPerson";
		onTheMove = true;
		descending = true;
		gameController.StartElevator(timeToGoDown);
		while (t < 1) {
			t += Time.deltaTime / timeToGoDown;
			transform.position = Vector3.Lerp(currentPos, new Vector3(0, standPositionY, 0), t);
			yield return null;
		}
		onTheMove = false;
		descending = false;
		gameController.StopElevator();
		GoToBathroom();
	}
	
	public void FallDown() {
		StopAllCoroutines();
		gameController.StopElevator();
		StartCoroutine("FallToStart");
	}
	
	IEnumerator FallToStart() {
		Vector3 currentPos = transform.position;
		float t = 0f;
		onTheMove = true;
		gameController.StartElevator(0.25f);
		while (t < 1) {
			t += Time.deltaTime / 0.25f;
			transform.position = Vector3.Lerp(currentPos, new Vector3(0, standPositionY, 0), t);
			yield return null;
		}
		onTheMove = false;
		descending = false;
		gameController.StopElevator();
		GoToBathroom();
	}
	
	private Bathroom targetBathroom;
	private bool targetBathroomCorrect;
	
	private bool currentBathroomCorrect;
	
	public void SendToBathroom(Bathroom bathroom, bool correctBathroom) {
		targetBathroom = bathroom;
		targetBathroomCorrect = correctBathroom;
		if (descending) {
			FallDown();
		}
		GoToBathroom();
	}
	
	private void GoToBathroom() {
		if (!onTheMove && targetBathroom != null) {
			StartCoroutine(MoveToBathroom());
		}
	}
	
	IEnumerator MoveToBathroom()
	{
		Vector3 targetPosition = GameController.CameraToWorld(new Vector3(targetBathroom.position.x, targetBathroom.position.y));
		float targetScale = targetBathroom.scale;
		Door currentDoor = targetBathroom.door;
		currentBathroomCorrect = targetBathroomCorrect;
		targetBathroom = null;
		onTheMove = true;
		Vector3 currentPos = new Vector3(0, standPositionY, 0);
		Vector3 currentScale = transform.localScale;
		float t = 0f;
		gameController.StartConveyor();
		while (t < 1) {
			t += Time.deltaTime / (timeToGoDown / 2);
			if (t > 0.5f) {
				currentDoor.Open ();
			}
			transform.position = Vector3.Lerp(currentPos, targetPosition, t);
			transform.localScale = currentScale * Mathf.Lerp(1, targetScale, t);
			yield return null;
		}
		gameController.StopConveyor();
		if (currentBathroomCorrect) 
		{
			GameObject.FindWithTag("PeeBar").GetComponent<PeeBar>().StateSwith(PeePower * 60);
			currentDoor.Close();
			transform.tag = "Person";
			Destroy (gameObject, 1);
			yield return null;
			gameController.SummonNextPerson();
		} 
		else 
		{
			PlayAwnser (3);
			currentPos = transform.position;
			t = 0f;
			gameController.StartConveyor();
			while (t < 1) {
				t += Time.deltaTime / (timeToGoDown / 2);

				if (t > 0.5) {
					currentDoor.Close();
				}
				transform.position = Vector3.Lerp(currentPos, new Vector3(0, standPositionY, 0), t);
				transform.localScale = new Vector3(1, 1, 1) * Mathf.Lerp(targetScale, 1, t);
				yield return null;
			}
			gameController.StopConveyor();
			onTheMove = false;
			GoToBathroom();
		}
	}
}

