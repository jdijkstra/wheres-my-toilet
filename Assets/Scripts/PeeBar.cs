﻿using UnityEngine;
using System.Collections;

public class PeeBar : MonoBehaviour {

	// Use this for initialization
	public int Duration = 100;
	private int currentTime = 0;

	private GameObject GameController;

	void Start () 
	{
		Duration = Duration * 60;
		currentTime = Duration;
	}

	void FixedUpdate()
	{
		currentTime--;

		float temp = (float)currentTime / (float)Duration;
		transform.localScale = new Vector3 (1,temp,1);
		if (currentTime < 0) 
		{
			GameController = GameObject.FindWithTag ("GameController") as GameObject;
			GameController.GetComponent<GameController> ().EndGame ();
			Debug.Log ("blie");
		}

	}

	public void StateSwith(int increase) 
	{
		if(increase > 0)
		{
			audio.Play();
		}
		currentTime = currentTime + increase;
	}
}
