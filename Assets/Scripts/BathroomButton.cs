﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Bathroom {
	public Vector2 position;
	public float scale;
	public Door door;
}

public class BathroomButton : MonoBehaviour {

	public GameController gameController;

	public enum BathroomGender { Male, Female, Neutral }

	public BathroomGender gender = BathroomGender.Male;

	public Bathroom bathroom;

	void OnMouseUp () {
		Debug.Log(gender);
		GameObject personObject = GameObject.FindGameObjectWithTag("CurrentPerson");
		if (personObject != null) {
			Person person = personObject.GetComponent<Person>();
			if (person != null) {
				person.SendToBathroom(bathroom, rightBathroom((Person) person));
			}
		}
	}

	public bool rightBathroom(Person person) {
		switch (gender) {
		case BathroomGender.Male: return person.gender < 35;
		case BathroomGender.Neutral: return person.gender >= 35 && person.gender <= 65;
		case BathroomGender.Female: return person.gender > 65;
		default: return false;
		}
	}
}
